package com.bellver.prog.A7;

public class MainA7 {
     public static void main(String[] args) throws CounterNotInitializatedException{

         Profiler profiler = new Profiler();

         profiler.start("A1");

         for (int x = 0; x<10; x++){
             System.out.print(x);
         }

         System.out.println();

         profiler.stop("A2");

         profiler.showAmmountTime("A1");


     }
}
