package com.bellver.prog.A7;


import java.util.ArrayList;

public class Profiler {

    private ArrayList<Record> records;

    public Profiler() {

        this.records = new ArrayList<>();
    }

    public void start(String id){

        records.add(new Record(id));

    }

    public void stop(String id) throws  CounterNotInitializatedException{

        assertId(id);
        getRecord(id).finish();

    }

    private Record getRecord(String id){

            for (Record record: records) {

                if (record.getId() == id){
                    return record;
                }

            }

            return null;
    }

    public void showAmmountTime(String id){

        System.out.print("Tiempo de ejecucion: (hh:mm:ss:nn) " + getRecord(id).getAmmountTime());
    }

    public void assertId(String id) throws CounterNotInitializatedException {

            if(getRecord(id) == null){

             throw new CounterNotInitializatedException();

            }

    }

}
