package com.bellver.prog.A7;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


public class Record {

    private String id;

    private LocalDateTime start;

    private LocalDateTime finish;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss:nn");


    public Record(String id) {

        this.start = LocalDateTime.now();
        this.id = id;
    }

    public void finish(){

        this.finish = LocalDateTime.now();

    }

    public LocalTime getAmmountTime(){

      Duration aux = Duration.between(start,finish);

      int hour = (int) aux.getSeconds()/3600;
      int minutes = (int) aux.getSeconds()/60;
      int seconds = (int) aux.getSeconds();
      int nano = aux.getNano();

      return LocalTime.of(hour,minutes,seconds,nano);

    }


    public String getId() {

        return id;

    }
}
