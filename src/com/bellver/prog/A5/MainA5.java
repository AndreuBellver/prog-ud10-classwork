package com.bellver.prog.A5;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRulesException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class MainA5 {

    public static Scanner scanner;

    public static void main (String[] args){


        getFranjasHorarias();


    }
    
    public static void getFranjasHorarias() {

        scanner = new Scanner(System.in);

        int contador = 1;

        for (String zone : ZoneId.getAvailableZoneIds()) {

            System.out.println(contador + zone);
            contador++;

            if (contador == 10) {
                break;
            }
        }

        try {
            System.out.println("A que franja horaria quieres transformar:");
            String franja = scanner.next();

            ZonedDateTime ldt = ZonedDateTime.now();

            ZonedDateTime ldtZoned = ldt.withZoneSameInstant(ZoneId.of(franja));
            System.out.println(franja + ": " + ldtZoned.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));

        } catch (ZoneRulesException e) {

            System.out.println("\n\nInvalid Franka Horaria.\n\n");
            getFranjasHorarias();
        }

    }

    public static boolean isValidFranja(String zone){

        return zone.matches(".+/.+");

    }
}
