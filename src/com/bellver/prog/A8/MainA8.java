package com.bellver.prog.A8;

public class MainA8 {

    public static void main(String[] args) {

        int[] newArray = getRandomArray(500);



        System.out.println("Antes de ordenar:");
        showArray(newArray);

        test1(newArray);


  //      System.out.println("Despues de ordenar(QuickSort):");
  //      showArray(quickSort(newArray.clone()));



    }

    public static void test1 (int[] newArray){

        System.out.println("\n\nDespues de ordenar(BubbleSort):");

        RecordA8 sort1 = new RecordA8("BubbleSort");

        showArray(bubbleSort(newArray.clone()));

        sort1.finish();

        System.out.println("\nTiempo = [" + sort1.getAmmountTime() + "].");
    }

    public static void test2 (int[] newArray){

        System.out.println("\n\nDespues de ordenar(QuickSort):");

        RecordA8 sort2 = new RecordA8("QuickSort");

        showArray(quickSort(newArray.clone()));

        sort2.finish();

        System.out.println("\nTiempo = [" + sort2.getAmmountTime() + "].");
    }


    public static int[] getRandomArray(int lenght){

        int[] aux = new int[lenght];

        for (int x = 0; x < lenght; x++){

            aux[x] = (int) ((Math.random())*1000);
        }

        return aux;
    }

    public static void showArray(int[] array){

        for (int y = 0; y < array.length; y++){
            System.out.print(array[y] + ",");
        }

    }

    public static int[] bubbleSort(int [] array){



            int n = array.length;
            int temp;

            for(int i=0; i < n; i++){

                for(int j=1; j < (n-i); j++){

                    if(array[j-1] > array[j]){

                        temp = array[j-1];
                        array[j-1] = array[j];
                        array[j] = temp;

                    }

                }

            }


        return array;

    }

    public static int[] quickSort(int[] array){

        RecordA8 sort2 = new RecordA8("QuickSort");

        int pivote = (array.length/2);

        final int initizlPosition = 0;
        final int endPosition = 499;

        partition(array,initizlPosition,pivote);
        partition(array,pivote,endPosition);

        sort2.finish();

        return array;

    }

    public static void partition(int[] array, int begin, int end){

        int pivot = array[end];
        int i = begin-1;

        for (int x = begin; x < end; x++){

            if (array[x] <= pivot){
                i++;
            }

            int swap = array[i];
            array[i] = array[x];
            array[x] = swap;

        }
    }

}
