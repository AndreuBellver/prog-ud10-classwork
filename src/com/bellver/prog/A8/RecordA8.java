package com.bellver.prog.A8;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;


public class RecordA8 {

    private String id;

    private LocalDateTime start;

    private LocalDateTime finish;


    public RecordA8(String id) {

        this.start = LocalDateTime.now();
        this.id = id;
    }

    public void finish(){

        this.finish = LocalDateTime.now();

    }

    public LocalTime getAmmountTime(){

      Duration aux = Duration.between(start,finish);

      int hour = (int) aux.getSeconds()/3600;
      int minutes = (int) aux.getSeconds()/60;
      int seconds = (int) aux.getSeconds();
      int nano = aux.getNano();

      return LocalTime.of(hour,minutes,seconds,nano);

    }


    public String getId() {

        return id;

    }
}
