package com.bellver.prog.A8;

import java.util.Arrays;

public class QuickSort {

    private int arrayOfElements[];

    public QuickSort(int ...arrayOfElements) {
        this.arrayOfElements = arrayOfElements;
    }

    public void sort() {

        sort(0, arrayOfElements.length - 1);

    }
    private void sort(int start, int stop) {

        if ((stop - start) <= 1){
            return;
        }

        int pivotePosition = (start + stop) / 2;
        int pivoteValue = arrayOfElements[pivotePosition];

        swap(stop, pivotePosition);

        int smallPosition = -1;

        for (int i = 0; i < stop; i++) {

            if (arrayOfElements[i] < pivoteValue) {
                smallPosition++;
                swap(smallPosition,i);
            }

        }

        int correctPivotePosition = smallPosition + 1;
        swap(correctPivotePosition, stop);
        sort(start, correctPivotePosition-1);
        sort(correctPivotePosition + 1, stop);

    }

    private void swap(int left, int right) {

        int aux = arrayOfElements[left];
        arrayOfElements[left] = arrayOfElements[right];
        arrayOfElements[right] = aux;

    }

    @Override
    public String toString() {
        return "QuickSort{" +
                "arrayOfElements=" + Arrays.toString(arrayOfElements) +
                '}';
    }

    private void showPart(int left, int right) {

        for (int i = left; i <= right; i++){
            System.out.print(arrayOfElements[i]+",");
        }
        System.out.println();
    }
}
