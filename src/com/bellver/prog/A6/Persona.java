package com.bellver.prog.A6;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Persona {

    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private String email;
    private String phoneNumber;
    private LocalDateTime createdOn;
    private String password;
    private String salt;

    public Persona(String firstName, String lastName, LocalDate birthday, String email, String phoneNumber, String password, String salt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.createdOn = LocalDateTime.now();
        this.password = password;
        this.salt = salt;
    }

    public Period getAge(){

        Period age = Period.between(birthday, LocalDate.now());

        return age;
    }

    public boolean isNewUser(){

        Duration duration = Duration.between(createdOn,LocalDateTime.now());

        return (duration.toDays() < 15);

    }
}
