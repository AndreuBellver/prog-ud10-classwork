package com.bellver.prog.A1;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Formatter;
import java.util.Scanner;

public class MainA1 {

    public static Scanner scanner;

    public static void main (String[] args){

        runMenuElection(menu());

    }

    public static int menu(){
        scanner = new Scanner(System.in);

        System.out.println("Que quieres mostrar:");
        System.out.println("1.- Fecha Entera.");
        System.out.println("2.- Año.");
        System.out.println("3.- Mes.");
        System.out.println("4.- Dia.");

        return scanner.nextInt();
    }

    public static void runMenuElection(int election){

        switch (election){

            case 1:
                System.out.println(LocalDate.now().format(DateTimeFormatter.ISO_DATE));
                break;

            case 2:
                System.out.println(LocalDate.now().getYear());
                break;

            case 3:
               System.out.println(LocalDate.now().getMonth());
               break;

            case 4:
               System.out.println(LocalDate.now().getDayOfMonth());
        }

    }

}
