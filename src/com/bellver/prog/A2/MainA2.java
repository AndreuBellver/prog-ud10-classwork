package com.bellver.prog.A2;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


public class MainA2 {

    public static Scanner scanner;

    public static LocalDate aux;

    public static void main (String[] args){

        System.out.print(isBigger(getLocalDate("yyyy/MM/dd")));

    }

    public static LocalDate getLocalDate(String isoDate){

        try {

            scanner = new Scanner(System.in);

            System.out.println("Inserta una data (" + isoDate + "):");
            String insertDate = scanner.next();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(isoDate);
            aux = aux.parse(insertDate, formatter);

            return aux;

        }catch (DateTimeException e){

            throw e;
        }
    }

    public static String isBigger(LocalDate date){

        LocalDate aux = LocalDate.now();

        if (date.isBefore(aux)){

            return "La fecha introducida es anterior a la actual.";

        } else {

            return "La fecha introducida es posterior a la actual.";
        }
    }

    public static boolean isValidDate(String date){

        return date.matches("^([1-9]{4}\\/0[1-9]||1[0-2]\\/(0[1-9]||3[0-1]))$");
    }
}
