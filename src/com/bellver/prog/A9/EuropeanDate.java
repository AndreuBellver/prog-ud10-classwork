package com.bellver.prog.A9;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class EuropeanDate {

    private ZonedDateTime now;
    private final String[] cities = new String[]
            {"Europe/Budapest", "Europe/Dublin", "Atlantic/Reykjavik","Europe/Luxembourg",
            "Europe/Malta", "Europe/Amsterdam", "Europe/Stockholm"};
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public EuropeanDate() {

        this.now = ZonedDateTime.now();

    }

    public void showDates (){

        String aux = "Fechas y horas europeas:\n";


        for (int x = 0; x < cities.length; x++){

            ZoneId zone = ZoneId.of(cities[x]);
            ZonedDateTime zdtAux = now.withZoneSameInstant(zone);

            String dateAux = zdtAux.format(formatter);

            aux += cities[x] + "-> " + dateAux  + "\n";

        }

        System.out.print(aux);
    }

}
