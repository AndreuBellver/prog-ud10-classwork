package com.bellver.prog.A3;

import java.time.LocalDateTime;
import java.util.Scanner;

public class MainA3 {

    public static Scanner scanner;

    public static void main(String[] args){

       menu();


    }

    public static void menu(){

        scanner = new Scanner(System.in);

        System.out.println("1.- Añadir Dias");
        System.out.println("2.- Añadir Horas");
        System.out.println("2.- Añadir Minutos");

        int election = scanner.nextInt();

       runElection(election);

    }

    public static void runElection(int election){

        scanner = new Scanner(System.in);

        System.out.println("Cuantos quieres añadir:");
        int cantidad = scanner.nextInt();

        LocalDateTime now = LocalDateTime.now();

        switch (election) {

            case 1:

                now.plusDays(cantidad);
                break;

            case 2:

                now.plusHours(cantidad);
                break;

            case 3:

               now.plusMinutes(cantidad);

        }

        System.out.print(now);

    }

}
