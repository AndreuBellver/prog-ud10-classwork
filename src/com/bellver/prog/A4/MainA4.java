package com.bellver.prog.A4;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainA4 {

    public static Scanner scanner;
    public static LocalDateTime aux;

    public static void main(String[] args) {

        getLocalDateFormated("yyyy/MM/dd HH:mm:ss");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        System.out.println(aux.format(formatter));

    }

    public static LocalDateTime getLocalDateFormated(String isoDate) {

        scanner = new Scanner(System.in);

        System.out.println("Inserta una data: (YYYY/MM/DD hh:mm:ss)");
        String insertDateTime = scanner.nextLine();

        try {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(isoDate);

            aux = aux.parse(insertDateTime, formatter);

            return aux;

        } catch (DateTimeException e) {

            System.out.println("Fecha introducida: incorrecta.");
            throw e;
        }

    }

}
